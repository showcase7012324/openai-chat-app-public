import { Configuration, OpenAIApi } from 'openai';

export default async (req, res) => {
    const configuration = new Configuration({
        apiKey: process.env.OPENAI_API_KEY,
    });
  const openai = new OpenAIApi(configuration);

  const prompt = req.body.message;

  const gptResponse = await openai.createChatCompletion({
    model: 'gpt-3.5-turbo',
    messages: [{role: "user", content: prompt}],
  });

  if (gptResponse.data.choices[0].message!=undefined) 
    res.status(200).json({ message: gptResponse.data.choices[0].message });
  else
    res.status(500).json({ message: "Failed to generate a response" }) ;

};
