import { useState } from 'react'
import axios from 'axios'
import styles from '../styles/styles.module.css'
import parse from 'html-react-parser'
import globalcss from '../styles/global.css'

export default function Chat() {
  const [messages, setMessages] = useState([]);
  const [input, setInput] = useState('');

  const handleChange = (e) => {
    setInput(e.target.value);
  }

  const handleSubmit = async (e) => {
    e.preventDefault();

    const userMessage = {
      text: input,
      user: 'me',
    };

    setMessages((messages) => [...messages, userMessage]);
    setInput('');

    const response = await axios.post('/api/chat', { message: input });
    console.log(response);

    const aiMessage = {
      text: response.data.message.content,
      user: 'ai',
    };

    setMessages((messages) => [...messages, aiMessage]);
  }

  const addMessage = (message, index) => {
    //e.preventDefault();
    console.log("Hello moto");
    console.log(message);

    if(message.text.includes("```") && message.user === 'ai') {
      console.log("Found code for AI response");
      const code = message.text.replace(/```([\s\S]+?)```/g, "<pre><code>$1</code></pre>");
      const parsedCode = parse(code);
      return (
        <div key={index} className={styles.botMessage}>
          <strong className={styles.botIcon}>{message.user}</strong>
          <div className={styles.messageText}>
            {parsedCode}
          </div>
        </div>
      )
    }
    else if (message.user === 'ai') {
      console.log("Inside of return message for AI bot without code");
      return (
        <div key={index} className={styles.botMessage}>
          <strong className={styles.botIcon}>{message.user}</strong> 
          <div className={styles.chatMessage}>{message.text}</div>
        </div>
      )
    }
    else {
      console.log("Inside return message of user output");
      return (
        <div key={index} className={styles.userMessage}>
          <strong className={styles.userIcon}>{message.user}</strong>
          <div className={styles.chatMessage}>{message.text}</div>
        </div>
      )
    }
  }

  return (
    <div className={styles.chatContainer}>
      <h1>Chat with AI</h1>
      <div className={styles.chatBox}>
        {messages.map((message, index) => (
            addMessage(message, index)
        ))}
      </div>
      <form onSubmit={handleSubmit}>
        <input type="text" value={input} onChange={handleChange} />
        <button type="submit">Send</button>
      </form>
    </div>
  );
}


